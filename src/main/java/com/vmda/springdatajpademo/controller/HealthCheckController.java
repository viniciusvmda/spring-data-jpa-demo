package com.vmda.springdatajpademo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@CrossOrigin(origins = "*")
public class HealthCheckController {
	
	@ApiOperation("Check API health")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "API is on")
	})
	@GetMapping("/healthcheck")
	public ResponseEntity<String> getHealthCheck() {
		return ResponseEntity.status(HttpStatus.OK).body("ok");
	}

}
