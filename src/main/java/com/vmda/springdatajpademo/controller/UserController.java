package com.vmda.springdatajpademo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vmda.springdatajpademo.dto.UserDTO;
import com.vmda.springdatajpademo.dto.UserUploadDTO;
import com.vmda.springdatajpademo.service.UserService;

import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin(origins = "*")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@ApiOperation("Return a list of users")
	@GetMapping("/v1/users")
	public ResponseEntity<List<UserDTO>> getUsers() {
		return ResponseEntity.status(HttpStatus.OK).body(userService.findAllUsers());
	}
	
	@ApiOperation("Return users with given name")
	@GetMapping("/v1/users/{name}")
	public ResponseEntity<List<UserDTO>> getUserByName(@RequestParam String name) {
		return ResponseEntity.status(HttpStatus.OK).body(userService.findByName(name));
	}
	
	@ApiOperation("Save user")
	@PostMapping("/v1/users/")
	public ResponseEntity<String> saveUser(@RequestBody UserUploadDTO userDTO) {
		userService.saveUser(userDTO);
		return ResponseEntity.status(HttpStatus.CREATED).body("ok");
	}

}
