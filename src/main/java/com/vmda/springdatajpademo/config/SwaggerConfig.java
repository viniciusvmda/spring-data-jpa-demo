package com.vmda.springdatajpademo.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.RequestParameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class SwaggerConfig {

	@Bean
	public Docket apiDetail() {
		Docket docket = new Docket(DocumentationType.SWAGGER_2);

		docket.globalRequestParameters(globalParameterList()).select().apis(RequestHandlerSelectors.basePackage("com.vmda.springdatajpademo")).paths(PathSelectors.any())
				.build().apiInfo(this.apiInfo().build());

		return docket;
	}

	private ApiInfoBuilder apiInfo() {
		ApiInfoBuilder apiInfoBuilder = new ApiInfoBuilder();
		apiInfoBuilder.title("Spring Data JPA Demo API")
			.description("API for testing Spring Data JPA funcionalities");

		return apiInfoBuilder;

	}
	
	private List<RequestParameter> globalParameterList(){
		List<RequestParameter> parameterList = new ArrayList<>();	
		return parameterList;
	}
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}
}
