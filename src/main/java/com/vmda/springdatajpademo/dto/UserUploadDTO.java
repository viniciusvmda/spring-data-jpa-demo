package com.vmda.springdatajpademo.dto;

import lombok.Getter;

@Getter
public class UserUploadDTO {
	private String name;
	private String address;
}
