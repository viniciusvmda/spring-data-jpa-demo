package com.vmda.springdatajpademo.mapper;

import java.util.ArrayList;
import java.util.List;

import com.vmda.springdatajpademo.dto.UserDTO;
import com.vmda.springdatajpademo.dto.UserUploadDTO;
import com.vmda.springdatajpademo.repository.entity.UserEntity;

public class UserMapper {
	
	public UserEntity convertToEntity(UserUploadDTO userDTO) {
		var userEntity = new UserEntity();
		userEntity.setName(userDTO.getName());
		userEntity.setAddress(userDTO.getAddress());
		return userEntity;
	}

	public UserDTO convertToDTO(UserEntity userEntity) {
		var userDTO = new UserDTO();
		userDTO.setUserId(userEntity.getUserId());
		userDTO.setName(userEntity.getName());
		userDTO.setAddress(userEntity.getAddress());
		return userDTO;
	}

	public List<UserDTO> convertToDTOList(List<UserEntity> userEntityList) {
		List<UserDTO> dtoList = new ArrayList<>();
		userEntityList.forEach(entity -> {
			dtoList.add(convertToDTO(entity));
		});
		return dtoList;
	}
}
