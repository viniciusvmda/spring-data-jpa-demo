package com.vmda.springdatajpademo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vmda.springdatajpademo.repository.entity.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

	public List<UserEntity> findAllByName(String name);
	
}
