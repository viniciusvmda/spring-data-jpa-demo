package com.vmda.springdatajpademo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vmda.springdatajpademo.dto.UserDTO;
import com.vmda.springdatajpademo.dto.UserUploadDTO;
import com.vmda.springdatajpademo.mapper.UserMapper;
import com.vmda.springdatajpademo.repository.UserRepository;
import com.vmda.springdatajpademo.repository.entity.UserEntity;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;
	
	public List<UserDTO> findAllUsers() {
		var userMapper = new UserMapper();
		List<UserEntity> userEntityList = userRepository.findAll();
		List<UserDTO> userDTOList = userMapper.convertToDTOList(userEntityList);  
		return userDTOList;
	}

	public void saveUser(UserUploadDTO userDTO) {
		var userMapper = new UserMapper();
		UserEntity userEntity = userMapper.convertToEntity(userDTO);
		userRepository.save(userEntity);
	}

	public List<UserDTO> findByName(String name) {
		var userMapper = new UserMapper();
		List<UserEntity> userEntityList = userRepository.findAllByName(name);
		List<UserDTO> userDTOList = userMapper.convertToDTOList(userEntityList);
		return userDTOList;
	}
}
